<?php
require_once "exceptions/FileException.php";
class File
{



    private $file;

    private $fileName;



    public function __construct(string $fileName, array $arrTypes)
    {

        $this->file = $_FILES[$fileName];
        var_dump($this->file);
        $this->fileName = "";

        if (($this->file["name"] == "")) {

            throw new FileException("Debes especificar un fichero", 1);
        }

        if ($this->file["error"] !== UPLOAD_ERR_OK) {

            switch ($this->file["error"]) {

                case UPLOAD_ERR_INI_SIZE:

                case UPLOAD_ERR_FORM_SIZE:

                    throw new FileException("El archivo supera los 2 megas", 2);

                case UPLOAD_ERR_PARTIAL:

                    throw new FileException("Ya hay 20 archivos", 3);
                default:

                    throw new FileException("Error desconocido", 4);

                    break;
            }
        }

        if (in_array($this->file["type"], $arrTypes) === false) {

            throw new FileException("El formato no es valido pruebe con jpg, png o gif", 5);
        }
    }


    public function saveUploadFile(string $rutaDestino)
    {
       
        if (is_uploaded_file($this->file["tmp_name"]) === false) //Por seguridad
            throw new FileException("El archivo no se ha subido mediante un formulario");

        $this->fileName = $this->file["name"];
        $ruta = $rutaDestino . $this->fileName;
        if (is_file($ruta) === true) { //Si el fichero ya existe lo renombramos aleatoriamente
            $idUnico = time();
            $this->fileName = $idUnico . "_" . $this->fileName;
            $ruta = $rutaDestino . $this->fileName;
        }
        //echo $ruta;
        
        if (move_uploaded_file($this->file["tmp_name"], $ruta) === false) {
            throw new FileException("No se ha podido mover el fichero al destino especificado.");
        }
    }
    public function copyFile(string $origen, string $destino)
    {
        $rutaOrigen = $origen . $this->file['name'];
        $rutaDestino = $destino . $this->file['name'];

        if (!is_file($rutaOrigen)) {

            throw new FileException("No existe el fichero $origen.", 8);
        }

        if (is_file($rutaDestino)) {

            throw new FileException("El fichero $destino ya existe.", 9);
        }


        if (!copy($rutaOrigen, $rutaDestino)) {

            throw new FileException("No se ha podido copiar el fichero.", 10);
        }
    }
    public function getFileName(){return $this->file["name"];}
}
