<?php

require_once "utils/utils.php";
require_once "utils/File.php";
require_once "exceptions/FileException.php";
require_once "entity/ImagenGaleria.php";
require_once "database/Connection.php";
require_once "database/QueryBuilder.php";

//echo $_SERVER["PHP_SELF"];

try {
    $connection = DBConnect::make();
    $queryBuilder = new QueryBuilder($connection);

if ($_SERVER["REQUEST_METHOD"]==="POST") {
 
   

        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));

        

        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

        $imagen = new File("imagen", $tiposAceptados);     
       
         //var_dump ($imagen);

        $imagen->saveUploadFile(ImagenGaleria::RUTA_IMAGENES_GALLERY); 
      
        $imagen->copyFile(ImagenGaleria::RUTA_IMAGENES_GALLERY, ImagenGaleria::RUTA_IMAGENES_PORTFOLIO);
        
        $mensaje = "Enviado";

        $sql = "INSERT INTO imagenes (nombre, descripcion) VALUES (:nombre, :descripcion)";
        $pdoStatement = $connection->prepare($sql);
        $parameters = [":nombre" => $imagen->getFileName(),":descripcion" => $descripcion];
        if ($pdoStatement->execute($parameters) === false) {
            $errores [] = "No se a guardado en la base de datos";
        }else{
            $mensaje = "Se a guardado en la base de datos";
        }


}
    $imagenes = $queryBuilder->findAll("imagenes", "ImagenGaleria");
    } catch (FileException $fileException) {

        $errores [] = $fileException->getMessage();

    }   
    
    catch (QueryException $queryException) {

    $errores [] = $queryException->getMessage();

}   



require_once "views/galeria.view.php";

?>