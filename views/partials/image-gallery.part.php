<div id="<?php echo $IdCategory ?>" class="tab-pane <?php if($IdCategory == "category1") echo "active"; ?>">

  <div class="row popup-gallery">

    <?php foreach ($imagenes as $arte) : ?>

      <div class="col-xs-12 col-sm-6 col-md-3">

        <div class="sol">

          <img class="img-responsive" src="<?php echo $arte->getURLPortfolio(); ?>" alt="First category picture">

          <div class="behind">

            <div class="head text-center">

              <ul class="list-inline">

                <li>
                  <a class="gallery" href="<?php echo $arte->getURLGallery();  ?>" data-toggle="tooltip" data-original-title="Quick View">
                    <i class="fa fa-eye"></i>
                  </a>
                </li>
                <li>
                  <a href="#" data-toggle="tooltip" data-original-title="Click if you like it">
                    <i class="fa fa-heart"></i>
                  </a>
                </li>
                <li>
                  <a href="#" data-toggle="tooltip" data-original-title="Download">
                    <i class="fa fa-download"></i>
                  </a>
                </li>
                <li>
                  <a href="#" data-toggle="tooltip" data-original-title="More information">
                    <i class="fa fa-info"></i>
                  </a>
                </li>
              </ul>
            </div>
            <div class="row box-content">
              <ul class="list-inline text-center">
                <li><i class="fa fa-eye"></i> 100<?php //echo $arte->getURLGallery();  
                                                  ?> </li>
                <li><i class="fa fa-heart"></i> 100<?php //echo $arte->getURLGallery();  
                                                    ?> </li>
                <li><i class="fa fa-download"></i> 100<?php //echo $arte->getURLGallery();  
                                                      ?> </li>
              </ul>
            </div>
          </div>

        </div>
      </div>
    <?php endforeach; ?>
  </div>
</div>