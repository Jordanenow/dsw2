<?php


	$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : null;
	$email = isset($_POST['email']) ? $_POST['email'] : null;
	$subject = isset($_POST['subject']) ? $_POST['subject'] : null;
	$mensaje = isset($_POST['mensaje']) ? $_POST['mensaje'] : null;

	$errores = array();
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {


		if (!validaNombre($nombre)) {
			array_push($errores, 'El campo nombre es incorrecto.');
		}

		if (!validaEmail($email)) {
			array_push($errores, 'El campo email es incorrecto.');
		}

		if (!validaSubject($subject)) {
			array_push($errores, 'El campo subject es incorrecto.');
		}

		if (!validaMensaje($mensaje)) {
			array_push($errores, 'El campo mensaje es incorrecto.');
		}
	}

include("utils/utils.php"); 
require_once "views/contact.view.php";

?>
